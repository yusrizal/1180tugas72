<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html>
<html lang="id">
    <head>
        <title>Ubah</title>
    </head>
    <body>
        <h1>Ubah</h1>
        <form action="/ubah" method="post">
            <input type="hidden" name="hdnId" value="${data.key.id}">
            Nama : <input type="text" name="txtNama" value="${data.properties.Nama}"><br/>
            NIM	 : <input type="text" name="txtNim" value="${data.properties.Nim}"><br/>
            Email: <input type="text" name="txtEmail" value="${data.properties.Email}"><br/>
            NoHP : <input type="text" name="txtNoHP" value="${data.properties.NoHP}"><br/>
            Aktif: <select name="Aktif">
            		<option value="TRUE">Aktif </option>
            		<option value="FALSE">Tidak Aktif</option>
            	</select>
            <input type="submit" value="ubah">
        </form>
    </body>
</html>

